package com.artik;

import com.artik.models.input.*;
import com.artik.models.output.ClientDimension;
import com.artik.models.output.DateDimension;
import com.artik.models.output.VenteFait;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        /* Chargement des données depus les fichiers CSV sources */

        /* TODO: prendre en charge les autres sources, par exemple produit.csv
         *   1. Commencer avec produit.csv dont la classe ProduitInput est précodée
         *   2. Continuer avec par exemple Département*/

        List<VenteInput> ventesInput =
                VenteInput.readVenteInput("resources/ventes.csv", ";");

        TreeMap<String, ClientInput> clientsInput =
                ClientInput.readClientInput("resources/clients.csv", ";");
        ClientInput clientInput;

        TreeMap<String, VilleInput> villesInput =
                VilleInput.readVilleInput("resources/villes_france.csv", ",");

        TreeMap<String, PaysInput> paysListInput =
                PaysInput.readPaysInput("resources/pays.csv", ",");





        /* Modèle en étoile */

        /* TODO :
            1. Ajouter la dimension Produit
            2. Ajouter les autres dimensions identifiées
        */

        // Table de faits
        List<VenteFait> ventesFait = new ArrayList<>();
        // Tables de dimensions
        TreeMap<String, ClientDimension> clientsDimension = new TreeMap<>();
        TreeMap<Long, DateDimension> datesDimension = new TreeMap<>();


        for (VenteInput venteInput : ventesInput) {

            clientInput = clientsInput.getOrDefault(
                    venteInput.idClient,
                    ClientInput.defaultClient
            );

            // Alimentation des tables de dimensions
            clientsDimension.putIfAbsent(
                    clientInput.id,
                    new ClientDimension(clientInput)
            );

            datesDimension.putIfAbsent(
                    venteInput.dateVente.getTime(),
                    new DateDimension(venteInput.dateVente)
            );


            // Alimentation de la table de faits
            ventesFait.add(new VenteFait(venteInput, clientInput.id, venteInput.dateVente.getTime()));
        }


        /* Écriture des tables de faits et de dimensions dans des fichiers CSV */

        VenteFait.writeToCsv(ventesFait, "output/ventes-faits");
        DateDimension.writeToCsv(datesDimension.values(), "output/date-dimension");
        ClientDimension.writeToCsv(clientsDimension.values(), "output/client-dimension");


        /* Écriture du fichier csv à importer dans Elastic ! */

        VenteFait.writeToElasticCsv("output/elastic", ventesFait, datesDimension, clientsDimension);


    }
}
