package com.artik.models.output;

import com.artik.models.input.ClientInput;
import com.artik.models.input.VenteInput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

public class VenteFait {

    public int quantiteProduit;
    public String montantVendu;
    public String idClient;
    public long timestampVente;

    /* Ce constructeur contient les transformations entre les tables transactionnelles et la table de dimension */
    public VenteFait(VenteInput vente, String idClient, long timestampVente) {
        this.quantiteProduit = Integer.parseInt(vente.quantiteProduit);
        this.montantVendu = vente.montant;
        this.idClient = idClient;
        this.timestampVente = timestampVente;
    }

    /* Méthodes statiques pour l'écriture dans les fichiers CSV */

    public static void writeToCsv(List<VenteFait> ventes, String fileName) {

        try ( PrintWriter writer = new PrintWriter(new File(String.format("%s.csv", fileName)))) {

            writer.println("quantiteProduit,montantVendu,idClient,timestampVente");

            for (VenteFait vente : ventes) {
                writer.println(String.format("%s,%s,%s,%s",
                        vente.quantiteProduit, vente.montantVendu, vente.idClient, vente.timestampVente));
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }

    }

    public static void writeToElasticCsv(String fileName, Collection<VenteFait> ventes,
                                         TreeMap<Long, DateDimension> dates, TreeMap<String, ClientDimension> clients) {

        ClientDimension client;
        DateDimension date;

        try ( PrintWriter writer = new PrintWriter(new File(String.format("%s.csv", fileName)))) {


            writer.println("quantiteProduit,montantVendu,idClient,anneeDeNaissanceClient,genreClient,villeClient,paysClient,timestampVente,anneeVente,moisVente,jourVente");

            for (VenteFait vente : ventes) {

                client = clients.get(vente.idClient);
                date = dates.getOrDefault(
                        vente.timestampVente,
                        new DateDimension(new Date(0))
                );

                writer.println(String.format(
                        "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                        vente.quantiteProduit, vente.montantVendu,
                        client.id, client.anneeNaissance, client.genre, client.ville, client.pays,
                        date.timestamp, date.annee, date.mois,date.jour
                ));
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }
}
