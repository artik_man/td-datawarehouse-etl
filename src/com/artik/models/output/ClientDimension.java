package com.artik.models.output;

import com.artik.models.input.ClientInput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collection;

public class ClientDimension {

    public String id;
    public int anneeNaissance;
    public String genre;
    public String ville;
    public String pays;

    public ClientDimension(ClientInput client) {
        this.id = client.id;
        this.anneeNaissance = Integer.parseInt(client.anneeDeNaissance);
        this.genre = client.genre;
    }

    public static void writeToCsv(Collection<ClientDimension> clients, String fileName) {

        try ( PrintWriter writer = new PrintWriter(new File(String.format("%s.csv", fileName)))) {

            writer.println("idClient,anneeDeNaissance,genre,ville,pays");

            for (ClientDimension client : clients) {
                writer.println(String.format("%s,%s,%s,%s,%s", client.id, client.anneeNaissance, client.genre, client.ville, client.pays));
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }

    }

}
