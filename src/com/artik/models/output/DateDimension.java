package com.artik.models.output;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateDimension {

    public long timestamp;
    public int annee;
    public int mois;
    public int jour;

    public DateDimension(Date date) {
        Calendar calendarVente = new GregorianCalendar();
        calendarVente.setTime(date);
        this.annee = calendarVente.get(Calendar.YEAR);
        this.mois = calendarVente.get(Calendar.MONTH) + 1;
        this.jour = calendarVente.get(Calendar.DAY_OF_MONTH);
        this.timestamp = date.getTime();
    }

    public static void writeToCsv(Collection<DateDimension> dates, String fileName) {

        try ( PrintWriter writer = new PrintWriter(new File(String.format("%s.csv", fileName)))) {

            writer.println("timestamp,annee,mois,jour");

            for (DateDimension date : dates) {
                writer.println(String.format("%s,%s,%s,%s", date.timestamp, date.annee, date.mois, date.jour));
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }

    }
}
