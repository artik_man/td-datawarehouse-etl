package com.artik.models.input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeMap;

public class ProduitInput {
    public String reference;
    public String nom;
    public String prix;
    public String desc;
    public String idCatalogue;

    public ProduitInput(String reference, String nom, String prix, String desc, String idCatalogue) {
        this.reference = reference;
        this.nom = nom;
        this.prix = prix;
        this.desc = desc;
        this.idCatalogue = idCatalogue;
    }

    public static TreeMap<String, ProduitInput> readProduitInput(String fileName) {
        String line;
        String splitBy = ",";

        TreeMap<String, ProduitInput> produits = new TreeMap<>();

        try  {
            //parsing a CSV file into BufferedReader class constructor
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            br.readLine();

            while ((line = br.readLine()) != null)  {
                String[] produit = line.split(splitBy);
                if (produit.length != 5) {
                    throw new IllegalArgumentException();
                }
                produits.putIfAbsent(produit[0], new ProduitInput(
                        produit[0],
                        produit[1],
                        produit[2],
                        produit[3],
                        produit[4]
                ));
            }

        } catch(IOException e) {
            e.printStackTrace();
        }

        return produits;
    }

}
