package com.artik.models.input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.TreeMap;

public class ClientInput {
    public String id;
    public String nom;
    public String prenom;
    public String email;
    public String telephone;
    public String anneeDeNaissance;
    public String genre;
    public String codecommune;

    public ClientInput(String id, String nom, String prenom, String email, String telephone, String anneeDeNaissance, String genre, String codecommune) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.telephone = telephone;
        this.anneeDeNaissance = anneeDeNaissance;
        this.genre = genre;
        this.codecommune = codecommune;
    }

    public static TreeMap<String, ClientInput> readClientInput(String fileName, String splitBy) {
        String line;

        TreeMap<String, ClientInput> clients = new TreeMap<>();

        try  {
            //parsing a CSV file into BufferedReader class constructor
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            br.readLine();

            while ((line = br.readLine()) != null)  {
                String[] client = line.split(splitBy);
                if (client.length != 8) {
                    System.err.println(String.format("line of length %s with first element %s", client.length, client[0]));
                    throw new IllegalArgumentException();
                }
                clients.putIfAbsent(client[0], new ClientInput(
                        client[0],
                        client[1],
                        client[2],
                        client[3],
                        client[4],
                        client[5],
                        client[6],
                        client[7]
                ));
            }

        } catch(IOException e) {
            e.printStackTrace();
        }

        return clients;
    }

    public static ClientInput defaultClient = new ClientInput(
            "0",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    );

    @Override
    public String toString() {
        return "ClientInput{" +
                "id='" + id + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", anneeDeNaissance=" + anneeDeNaissance +
                ", genre='" + genre + '\'' +
                ", codecommune='" + codecommune + '\'' +
                '}';
    }
}
