package com.artik.models.input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeMap;

public class VilleInput {

    public String codeCommune;
    public String nom;
    public String codePostal;
    public float longitude;
    public float latittude;

    public VilleInput(String codeCommune, String nom, String codePostal, float longitude, float latittude) {
        this.codeCommune = codeCommune;
        this.nom = nom;
        this.codePostal = codePostal;
        this.longitude = longitude;
        this.latittude = latittude;
    }

    public static TreeMap<String, VilleInput> readVilleInput(String fileName, String splitBy) {
        String line;

        TreeMap<String, VilleInput> villes = new TreeMap<>();

        try  {
            //parsing a CSV file into BufferedReader class constructor
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            br.readLine();

            while (null != (line = br.readLine()))  {
                String[] ville = line.split(splitBy);
                if (ville.length != 5) {
                    System.err.println(String.format("length %s with 1st param %s", ville.length, ville[0]));
                    throw new IllegalArgumentException();
                }
                villes.putIfAbsent(ville[0], new VilleInput(
                        ville[0],
                        ville[1],
                        ville[2],
                        Float.parseFloat(ville[3]),
                        Float.parseFloat(ville[4])
                ));
            }

        } catch(IOException e) {
            e.printStackTrace();
        }

        return villes;
    }

    @Override
    public String toString() {
        return "VilleInput{" +
                "codeCommune='" + codeCommune + '\'' +
                ", nom='" + nom + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", longitude=" + longitude +
                ", latittude=" + latittude +
                '}';
    }
}
