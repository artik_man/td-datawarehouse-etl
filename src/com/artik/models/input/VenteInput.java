package com.artik.models.input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VenteInput {

    public String idVente;
    public String quantiteProduit;
    public String montant;
    public String idClient;
    public String idMagasin;
    public String referenceProduit;
    public Date dateVente;

    public VenteInput(String idVente, String quantiteProduit, String montant, String idClient, String idMagasin, String referenceProduit, Date dateVente) {
        this.idVente = idVente;
        this.quantiteProduit = quantiteProduit;
        this.montant = montant;
        this.idClient = idClient;
        this.idMagasin = idMagasin;
        this.referenceProduit = referenceProduit;
        this.dateVente = dateVente;
    }

    public static List<VenteInput> readVenteInput(String fileName, String splitBy) {
        String line;

        List<VenteInput> ventes = new ArrayList<>();

        try  {
            //parsing a CSV file into BufferedReader class constructor
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            br.readLine();

            while (null != (line = br.readLine()))  {
                String[] vente = line.split(splitBy);
                if (vente.length != 7) {
                    System.err.println(String.format("line of length %s with id %s", vente.length, vente[0]));
                    throw new IllegalArgumentException();
                }
                ventes.add(new VenteInput(
                        vente[0],
                        vente[1],
                        vente[2],
                        vente[3],
                        vente[4],
                        vente[5],
                        new Date(Long.parseLong(vente[6]))
                ));
            }

        } catch(IOException e) {
            e.printStackTrace();
        }

        return ventes;
    }

    @Override
    public String toString() {
        return "VenteInput{" +
                "quantiteProduit=" + quantiteProduit +
                ", montant=" + montant +
                ", dateVente=" + dateVente +
                ", idClient='" + idClient + '\'' +
                ", idMagasin='" + idMagasin + '\'' +
                ", referenceProduit='" + referenceProduit + '\'' +
                '}';
    }
}
