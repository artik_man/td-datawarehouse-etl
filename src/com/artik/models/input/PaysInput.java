package com.artik.models.input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeMap;

public class PaysInput {
    public String name;
    public   String alpha2;
    public    String alpha3;
    public String countrycode;
    public    String iso31662;
    public String region;
    public String subregion;
    public String intermediateregion;
    public String regioncode ;
    public String subregioncode;
    public String intermediateregioncode;


    public PaysInput(String name, String alpha2, String alpha3, String countrycode, String iso31662,
                     String region, String subregion, String intermediateregion, String regioncode,
                     String subregioncode, String intermediateregioncode) {
        this.name = name;
        this.alpha2 = alpha2;
        this.alpha3 = alpha3;
        this.countrycode = countrycode;
        this.iso31662 = iso31662;
        this.region = region;
        this.subregion = subregion;
        this.intermediateregion = intermediateregion;
        this.regioncode = regioncode;
        this.subregioncode = subregioncode;
        this.intermediateregioncode = intermediateregioncode;
    }


    public static TreeMap<String, PaysInput> readPaysInput(String fileName, String splitBy) {
        String line;

        TreeMap<String, PaysInput> paysList = new TreeMap<>();

        try  {
            //parsing a CSV file into BufferedReader class constructor
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            br.readLine();

            while ((line = br.readLine()) != null)  {
                String[] pays = line.split(splitBy);

                if (pays.length != 11) {
                    System.err.println(pays[0] + "" + pays.length);
                    throw new IllegalArgumentException();
                }

                paysList.putIfAbsent(pays[3], new PaysInput(
                        pays[0],
                        pays[1],
                        pays[2],
                        pays[3],
                        pays[4],
                        pays[5],
                        pays[6],
                        pays[7],
                        pays[8],
                        pays[9],
                        pays[10]
                ));
            }

        } catch(IOException e) {
            e.printStackTrace();
        }

        return paysList;
    }


    public static PaysInput defaultPays = new PaysInput("France","FR","FRA","250","ISO 3166-2:FR","Europe",
            "Western Europe","","150","155","");

    @Override
    public String toString() {
        return "PaysInput{" +
                "name='" + name + '\'' +
                ", alpha2='" + alpha2 + '\'' +
                ", alpha3='" + alpha3 + '\'' +
                ", countrycode='" + countrycode + '\'' +
                ", iso31662='" + iso31662 + '\'' +
                ", region='" + region + '\'' +
                ", ubregion='" + subregion + '\'' +
                ", intermediateregion='" + intermediateregion + '\'' +
                ", regioncode='" + regioncode + '\'' +
                ", subregioncode='" + subregioncode + '\'' +
                ", intermediateregioncode='" + intermediateregioncode + '\'' +
                '}';
    }
}
